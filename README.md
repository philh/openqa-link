# repo to enable salsa CI jobs to trigger openQA tests

## Steps required to enable this:

1. make sure that you're at least 'operator' on openqa.debian.net, and so can
   see the `manage API tokens` option on the user menu.
1. fork this repo into your own namespace
1. mark the `main` branch as protected
1. create some protected variables
   - OPENQA_KEY & OPENQA_SECRET

     these should contain a key/secret pair obtained from openQA's `manage API tokens`

   - SALSA_API_TOKEN

     This is required in order to create the linked jobs in gitlab pointing
     towards the jobs on openqa. If you don't bother setting it, things will
     still work, but you won't get shown the jobs in gitlab's webUI.
